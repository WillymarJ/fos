'use strict';

const Router = require(`koa-router`);
const auth = require(`../middlewares/authenticated`);

const AuthController = require(`../controllers/AuthController`);
const OAuthController = require(`../controllers/OAuthController`);
const MeController = require(`../controllers/MeController`);
const OrderController = require(`../controllers/OrderController`);

// Assign Controller
const MenuController = require(`../controllers/MenuController`);

module.exports = (app) => {
  const router = new Router({
    prefix: `/api/v1`,
  });

  router.get(`/ping`, (ctx, next) => {
    ctx.res.ok(`ok`);
  });

  // Auth
  router.post(`/register_customer`, AuthController.registerCustomer);

  // OAuth2
  router.post(`/login`, auth.isAuthenticatedPassword(), OAuthController.getTokenDirectPassword);
  router.post(`/create/oauth`, auth.isAuthenticatedApiBasic(), OAuthController.create);

  // Me
  router.get(`/me`, auth.isAuthenticatedAccessToken(), MeController.show);
  router.put(`/me`, auth.isAuthenticatedAccessToken(), MeController.update);

  // Menu
  router.post(`/menu`, auth.isAuthenticatedAccessToken(), auth.isAuthenticated([1]), MenuController.addMenu);
  router.get(`/menu`, auth.isAuthenticatedAccessToken(), auth.isAuthenticated([1, 3]), MenuController.listMenu);
  router.get(`/menu/:id`, auth.isAuthenticatedAccessToken(), auth.isAuthenticated([1, 3]), MenuController.showMenu);
  router.put(`/menu/:id`, auth.isAuthenticatedAccessToken(), auth.isAuthenticated([1]), MenuController.updateMenu);
  router.delete(`/menu/:id`, auth.isAuthenticatedAccessToken(), auth.isAuthenticated([1]), MenuController.deleteMenu);


  // Order
  router.post(`/order`, auth.isAuthenticatedAccessToken(), auth.isAuthenticatedAccessToken(), auth.isAuthenticated([3]), OrderController.order);
  app
        .use(router.routes())
        .use(router.allowedMethods());
};
