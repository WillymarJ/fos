'use strict';

const config = require(`config`);
const Sequelize = require(`sequelize`);

const sequelize = new Sequelize(
    config.database.postgres.database,
    config.database.postgres.user,
    config.database.postgres.password, {
      host: config.database.postgres.host,
      dialect: config.database.postgres.dialect,
      port: config.database.postgres.port,

      pool: {
        max: config.database.postgres.maxPool,
        min: config.database.postgres.minPool,
        idle: config.database.postgres.idleTime,
      },

      logging: false,
    });

module.exports = sequelize;
