'use strict';

// service
const Joi = require(`../services/Joi`);

// Models
const Menu = require(`../models/Menu`);

module.exports = {

  // Create Menu dishes
  addMenu: async(ctx, next) => {
    const schema = Joi.object().keys({
      name: Joi.string().required(),
      description: Joi.string().allow(null),
      images: Joi.string(),
      price: Joi.number(),
      durationCook: Joi.number(),
      isAvailable: Joi.boolean().default(true),
    });
    try {
      const params = await Joi.validate(ctx.request.body, schema);
      const createMenu = await Menu.create({
        name: params.name,
        description: params.description,
        images: params.images,
        price: params.price,
        durationCook: params.durationCook,
        isAvailable: params.isAvailable,
      });
      ctx.res.ok(createMenu);
    } catch (error) {
      return next(error);
    }
  },


  listMenu: async(ctx, next) => {
    const offset = ctx.query.offset || 0;
    const limit = ctx.query.limit || 10;
    const conditions = {
      order: [
            [`name`, `asc`],
      ],
      offset: offset * limit,
      limit,
    };

    try {
      const list = await Menu.findAll(conditions);
      ctx.res.ok(list);
    } catch (error) {
      return next(error);
    }
  },

  showMenu: async(ctx, next) => {
    try {
      const dataMenu = await Menu.findOne({
        where: {
          id: ctx.params.id,
        },
      });
      if (!dataMenu) return ctx.res.notFound();
      ctx.res.ok(dataMenu);
    } catch (error) {
      return next(error);
    }
  },

  deleteMenu: async(ctx, next) => {
    try {
      const deleteMenu = await Menu.destroy({
        where: {
          id: ctx.params.id,
        },
      });
      if (deleteMenu === 0) {
        ctx.res.notFound();
      } else {
        ctx.res.noContent();
      }
    } catch (error) {
      return next(error);
    }
  },

  updateMenu: async(ctx, next) => {
    const schema = Joi.object().keys({
      name: Joi.string().required(),
      description: Joi.string().allow(null),
      images: Joi.string(),
      price: Joi.number(),
      durationCook: Joi.number(),
      isAvailable: Joi.boolean().default(true),
    });

    try {
      const params = await Joi.validate(ctx.request.body, schema);
      const findeMenu = await Menu.findOne({
        where: {
          id: ctx.params.id,
        },
      });

      if (!findeMenu) return ctx.res.notFound();

      findeMenu.name = params.name;
      findeMenu.description = params.description;
      findeMenu.images = params.images;
      findeMenu.price = params.price;
      findeMenu.durationCook = params.durationCook;
      findeMenu.isAvailable = params.isAvailable;

      const saveMenu = await findeMenu.save();
      ctx.res.ok(saveMenu);
    } catch (error) {
      return next(error);
    }
  },

};
