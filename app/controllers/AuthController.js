'use strict';

// Lib
const moment = require(`moment-timezone`);
const phone = require(`phone`);
const bcrypt = require(`bcrypt`);
const _ = require(`lodash`);

// Models
const User = require(`../models/User`);
const Apotek = require(`../models/Apotek`);

// Services
const Utils = require(`../services/Utils`);

module.exports = {

  /**
  * @api {post} /register_customer Create a Customer
  * @apiName Register Customer
  * @apiGroup Auth
  * @apiVersion  0.11.1
  * @apiPermission Customer
  *
  * @apiParam {String} [fullname]  Mandatory fullname of the User.
  * @apiParam {String} [email]  Mandatory email of the User.
  * @apiParam {String} [username]  Mandatory username of the User.
  * @apiParam {String} [password]  Mandatory password of the User.
  * @apiParam {String} [handphone]  Mandatory handphone of the User.
  * @apiParam {String} [address]  Mandatory address of the User.
  * @apiParam {String} [gender]  Mandatory gender of the User.
  * @apiParam {String} [date_of_birth (YYYY-MM-DD)]  Mandatory date_of_birth of the User.
  *
  * @apiSuccess {String} id id of the User.
  * @apiSuccess {String} email  email of the User.
  * @apiSuccess {String} username  username of the User.
  * @apiSuccess {String} handphone  handphone of the User.
  * @apiSuccess {String} address  address of the User.
  * @apiSuccess {String} gender  gender of the User.
  * @apiSuccess {String} date_of_birth  date_of_birth of the User.
  */
  registerCustomer: async(ctx, next) => {
    if (ctx.request.body.handphone) {
      if (ctx.request.body.handphone.toLowerCase().replace(/ /g, ``).length < 9) {
        ctx.res.badRequest(`Panjang minimal handphone adalah 9 digit`);
      }

      if (ctx.request.body.handphone.indexOf(0) === 0 || ctx.request.body.handphone.indexOf(0) === `0`) {
        ctx.request.body.handphone = `${ctx.request.body.handphone.substr(0, 0)}+62${ctx.request.body.handphone.substr(1)}`;
      }

      if (phone(ctx.request.body.handphone, ``).length === 0) {
        ctx.res.badRequest(`Format handphone tidak valid`);
      }
    }

    if (Utils.checkNotNullValue(ctx.request.body.date_of_birth)) {
      if (!moment(ctx.request.body.date_of_birth, `YYYY-MM-DD`, true).isValid()) {
        ctx.res.badRequest(`Format tanggal lahir tidak valid`);
      }
    }

    // Find email
    const dataUser = await User.findOne({
      where: {
        email: ctx.request.body.email,
      },
    });

    if (!dataUser) {
      const dataCustomer = {
        role_id: 3,
        user_status_id: 1,
        fullname: ctx.request.body.fullname,
        email: ctx.request.body.email,
        username: ctx.request.body.username,
        password: bcrypt.hashSync(ctx.request.body.password, 10),
        handphone: ctx.request.body.handphone,
        address: ctx.request.body.address,
        role_id_all: [3],
        gender: ctx.request.body.gender,
        created_at: Utils.getTimeEpoch(),
        updated_at: Utils.getTimeEpoch(),
      };

      if (Utils.checkNotNullValue(ctx.request.body.date_of_birth)) {
        dataCustomer.date_of_birth = `${ctx.request.body.date_of_birth} 00:00:00+00`;
      }

      ctx.res.created(await User.create(dataCustomer));
    } else {
      if (_.intersection([2, 3], dataUser.role_id_all).length === 2) {
        return ctx.res.badRequest(`Email address already in use!`);
      }

      // Update user
      dataUser.role_id_all = [2, 3];
      dataUser.fullname = ctx.request.body.fullname;
      dataUser.username = ctx.request.body.username;
      dataUser.password = bcrypt.hashSync(ctx.request.body.password, 10);
      dataUser.handphone = ctx.request.body.handphone;
      dataUser.address = ctx.request.body.address;
      dataUser.gender = ctx.request.body.gender;

      return ctx.res.created(await dataUser.save());
    }
  },

  /**
  * @api {post} /register_apotek Create apotek and user
  * @apiName RegisterApotek
  * @apiGroup Auth
  * @apiVersion  0.11.1
  * @apiPermission Admin
  *
  * @apiParam {String} [fullname]  Mandatory fullname of the User.
  * @apiParam {String} [email]  Mandatory email of the User.
  * @apiParam {String} [password]  Mandatory password of the User.
  * @apiParam {String} [handphone]  Mandatory handphone of the User.
  * @apiParam {String} [address_apotek]   address_apotek of the Apotek.
  * @apiParam {String} [name_apotek]   name_apotek of the Apotek.
  * @apiParam {String} [no_telp_apotek]   no_telp_apotek of the Apotek.
  * @apiParam {String} [lat]   lat location of the Apotek.
  * @apiParam {String} [lng]   lng location of the Apotek.
  * @apiParam {String} [license]   license/surat ijin of the Apotek.
  *
  * @apiSuccess {String} Success.
  */
  registerApotek: async(ctx, next) => {
    if (ctx.request.body.handphone) {
      if (ctx.request.body.handphone.toLowerCase().replace(/ /g, ``).length < 9) {
        ctx.res.badRequest(`Panjang minimal handphone adalah 9 digit`);
      }

      if (ctx.request.body.handphone.indexOf(0) === 0 || ctx.request.body.handphone.indexOf(0) === `0`) {
        ctx.request.body.handphone = `${ctx.request.body.handphone.substr(0, 0)}+62${ctx.request.body.handphone.substr(1)}`;
      }

      if (phone(ctx.request.body.handphone, ``).length === 0) {
        ctx.res.badRequest(`Format handphone tidak valid`);
      }
    }

    if (!moment(ctx.request.body.date_of_birth, `YYYY-MM-DD`, true).isValid()) {
      ctx.res.badRequest(`Format tanggal lahir tidak valid`);
    }

    // Find email
    const dataUser = await User.findOne({
      where: {
        email: ctx.request.body.email,
      },
    });

    let userId;

    if (!dataUser) {
      const dataCustomer = {
        role_id: 2,
        user_status_id: 1,
        fullname: ctx.request.body.fullname,
        email: ctx.request.body.email,
        password: bcrypt.hashSync(ctx.request.body.password, 10),
        handphone: ctx.request.body.handphone,
        role_id_all: [2],
        created_at: Utils.getTimeEpoch(),
        updated_at: Utils.getTimeEpoch(),
      };

      if (Utils.checkNotNullValue(ctx.request.body.date_of_birth)) {
        dataCustomer.date_of_birth = `${ctx.request.body.date_of_birth} 00:00:00+00`;
      }

      const customer = await User.create(dataCustomer);

      userId = customer.id;
    } else {
      if (_.intersection([2, 3], dataUser.role_id_all).length === 2) {
        return ctx.res.badRequest();
      }

      // Update user
      userId = dataUser.id;
      dataUser.role_id_all = [2, 3];

      await dataUser.save();
    }

    let wallet = 0;
    let deposit = 0;

    if (Utils.checkNotNullValue(ctx.req.user)) {
      if ([1] && _.intersection([1], ctx.req.user.role_id_all).length !== 0) {
        wallet = ctx.request.body.wallet;
        deposit = ctx.request.body.deposit;
      }
    }

    const dataApotek = {
      user_id: userId,
      name: ctx.request.body.name,
      address: ctx.request.body.address_apotek,
      no_telp: ctx.request.body.no_telp_apotek,
      password: bcrypt.hashSync(ctx.request.body.password, 10),
      lat: ctx.request.body.lat,
      lng: ctx.request.body.lng,
      license: ctx.request.body.license,
      wallet,
      deposit,
    };
    dataApotek.geom = {
      type: `Point`,
      coordinates: [ctx.request.body.lng, ctx.request.body.lat],
      crs: { type: `name`, properties: { name: `EPSG:4326` } },
    };

    const saveDataApotek = await Apotek.create(dataApotek);

    ctx.res.created(saveDataApotek.get());
  },


  /**
  *
  * @api {patch} /change_orgot_password Forgot password for user
  * @apiName Forgot Password
  * @apiGroup Auth
  * @apiVersion  0.11.1
  * @apiPermission Customer, Apotek
  *
  * @apiParam  {String} code Code user
  *
  * @apiParamExample  {type} Request-Example:
   {
     "code":"596337",
     "password":"12345678",
   }
  *
  *
  * @apiSuccessExample {type} Success-Response:
    {
      "meta": {
          "code": 200,
          "status": true,
          "messages": "Ok"
      },
      "data": "Your password has been change"
    }
  *
  */
  updateNewPassword: async(ctx, next) => {
    const missingParams = Utils.missingProperty(ctx.request.body, [`code`, `password`]);

    // Check Params
    if (missingParams) {
      return ctx.res.badRequest(`Missing parameter ${missingParams}`);
    }

    const dataForgotPassword = await ForgotPassword.findOne({
      where: {
        code: ctx.request.body.code,
        is_active: `t`,
      },
    });

    if (!dataForgotPassword) {
      return ctx.res.ok(`Code verification is failed`);
    }

    if (dataForgotPassword.is_active === `f`) {
      return ctx.res.ok(`Code verification already used`);
    }

    if (dataForgotPassword.expired_in < Utils.getTimeEpoch()) {
      return ctx.res.ok(`Code verification expired`);
    }

    // Create forgot password
    const dataUser = await User.find({
      where: {
        id: dataForgotPassword.user_id,
      },
    });

    dataUser.password = bcrypt.hashSync(ctx.request.body.password, 10);
    dataUser.updated_at = Utils.getTimeEpoch();

    dataForgotPassword.is_active = false;

    await dataUser.save();
    await dataForgotPassword.save();

    ctx.res.ok(`Your password has been change`);
  },

};
