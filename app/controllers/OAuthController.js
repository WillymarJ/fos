'use strict';

// Lib
const moment = require(`moment-timezone`);
const oauth2orize = require(`oauth2orize-koa`);
const passport = require(`koa-passport`);
const Promise = require(`bluebird`);
const bcrypt = Promise.promisifyAll(require(`bcrypt`));
const _ = require(`lodash`);

// create OAuth 2.0 server
const server = oauth2orize.createServer();

// Models
const OAuthClient = require(`../models/OAuthClient`);
const User = require(`../models/User`);
const OAuthAccessToken = require(`../models/OAuthAccessToken`);
const OAuthRefreshToken = require(`../models/OAuthRefreshToken`);
const Apotek = require(`../models/Apotek`);

// Services
const Utils = require(`../services/Utils`);

/* Resource owner password flow. */
server.exchange(oauth2orize.exchange.password(async(client, username, password, scope, req) => {
  // Check username or email
  let isEmail = false;

  // Check email regex
  if (Utils.checkRegexEmail(username)) {
    isEmail = true;
  }

  let dataUser;

  if (isEmail) {
    // Check with email
    dataUser = await User.findOne({ where: { email: username } });
  } else {
    // Check with username
    dataUser = await User.findOne({ where: { username } });
  }

  if (dataUser) {
    const isValid = await bcrypt.compareAsync(password, dataUser.password);

    // Check Chef or user
    if (client.id === 2) {
      if (_.intersection([2], dataUser.role_id_all).length === 0) {
        const err = new Error(`Your not Chef`);
        err.status = 401;
        throw err;
      }

      const dataApotek = await Apotek.findOne({ where: {
        user_id: dataUser.id,
      } });

      const isValidPasswordApotek = await bcrypt.compareAsync(password, dataApotek.password);

      if (!isValidPasswordApotek) {
        const err = new Error(`Username or Password incorrect`);
        err.status = 401;
        throw err;
      }
    } else if (!isValid) {
      // Check user
      if (client.id === 1) {
        if (_.intersection([3], dataUser.role_id_all).length === 0) {
          const err = new Error(`Your not user`);
          err.status = 401;
          throw err;
        }
      }
      const err = new Error(`Username or Password incorrect`);
      err.status = 401;
      throw err;
    }


    // Check user verified
    if (dataUser.user_status_id !== 1) {
      const err = new Error(`Account anda belum terverifikasi`);
      err.status = 401;
      throw err;
    }

    // Search data access token
    const accessToken = await OAuthAccessToken.findOne({ where: { user_id: dataUser.id, client_id: client.id } });

    if (accessToken) {
      // Remove access token
      await OAuthAccessToken.destroy({ where: { user_id: dataUser.id, client_id: client.id } });
    }

    // Search data refresh token
    const refreshToken = await OAuthRefreshToken.findOne({ where: { user_id: dataUser.id, client_id: client.id } });

    if (refreshToken) {
      // Remove refresh token
      await OAuthRefreshToken.destroy({ where: { user_id: dataUser.id, client_id: client.id } });
    }

    const expiredIn = moment()
            .tz(`Asia/Jakarta`)
            .add(1, `years`).unix() + (3600 * 7);

    const dataAccessToken = {
      access_token: Utils.uid(128),
      client_id: client.id,
      user_id: dataUser.get(`id`),
      expired_in: expiredIn,
      created_at: Utils.getTimeEpoch(),
      updated_at: Utils.getTimeEpoch(),
    };

    const saveAccessToken = await OAuthAccessToken.create(dataAccessToken);

    const dataRefreshToken = {
      refresh_token: Utils.uid(128),
      client_id: client.id,
      user_id: dataUser.get(`id`),
      expired_in: expiredIn,
      created_at: Utils.getTimeEpoch(),
      updated_at: Utils.getTimeEpoch(),
    };

    const saveRefreshToken = await OAuthRefreshToken.create(dataRefreshToken);

    // Insert access token fcm
    const tokenFcm = req.access_token_fcm || ``;
    const onlineFrom = `android`;
    const dataNotification = [];


    if (tokenFcm !== ``) {
      if (Utils.checkNotNullValue(dataUser.token_fcm) && dataUser.token_fcm.lengt > 0) {
        for (let i = 0, length = dataUser.token_fcm.length; i < length; i += 1) {
          if (dataUser.token_fcm.key === onlineFrom) {
            dataNotification.push({
              key: onlineFrom,
              value: tokenFcm,
            });
          } else {
            dataNotification.push({
              key: onlineFrom,
              value: tokenFcm,
            });
          }
        }
      } else {
        dataNotification.push({
          key: onlineFrom,
          value: tokenFcm,
        });
      }

      dataUser.token_fcm = dataNotification;

      await dataUser.save();
    }


    return [saveAccessToken.access_token, saveRefreshToken.refresh_token, {
      expired_in: expiredIn,
    }];
  }
  const err = new Error(`Username or Password incorrect`);
  err.status = 401;
  throw err;
}));

module.exports = {

  token: [
    passport.authenticate([`clientBasic`], {
      session: false,
    }),
    server.token(),
  ],

  bearer: [
    passport.authenticate(`accessToken`, {
      session: false,
    }),
  ],

    /**
   * @api {post} /login LoginCustomer
   * @apiName getTokenDirectPassword
   * @apiGroup Auth
   *
   * @apiParam {String} [username]  Mandatory Username of the User.
   * @apiParam {String} [password]  Mandatory Password of the User.
   * @apiParam {String} [grant_type]  Mandatory GrantType of the User.
   * @apiParam {String} [client_id]  Mandatory ClientId of the User.
   * @apiParam {String} [client_secret]  Mandatory ClientSecret of the User.
   *
   * @apiSuccess {String} access_token access_token of the User.
   * @apiSuccess {String} refresh_token  refresh_token of the User.
   * @apiSuccess {String} expired_in  expired_in of the User.
   * @apiSuccess {String} token_type  token_type of the User.
   */
  getTokenDirectPassword: server.token(),

  create: async(ctx, next) => {
    const oauthclient = {
      secret_id: ctx.request.body.secretID,
      name: ctx.request.body.name,
      website: ctx.request.body.website,
      redirect_uri: ctx.request.body.redirect,
      trusted: ctx.request.body.trusted,
      description: ctx.request.body.description,
      secret: ctx.request.body.secret,
    };
    const insertoauth = await OAuthClient.create(oauthclient);
    ctx.body = insertoauth;
  },

};
