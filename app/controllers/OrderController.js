'use strict';

// service
const Joi = require(`../services/Joi`);
const Utils = require(`../services/Utils`);


// Models
const Menu = require(`../models/Menu`);
const Order = require(`../models/Order`);

module.exports = {

  order: async(ctx, next) => {
    const schema = Joi.object().keys({
      userId: Joi.number(),
      dishesId: Joi.number(),
      portion: Joi.number(),
    });

    try {
      const params = await Joi.validate(ctx.request.body, schema);

      const menu = await Menu.findById(params.dishesId);

      if (!menu) return ctx.res.notFound();

      const createOrder = await Order.create({
        notrx: Utils.getTimeEpoch(),
        userId: params.userId,
        dishesId: menu.id,
        portion: params.portion,
        totalPrice: menu.price * params.portion,
        status: `on progres`,
        waitingTime: menu.durationCook * params.portion,
      });

      ctx.res.ok(createOrder);
    } catch (error) {
      return next(error);
    }
  },
};
