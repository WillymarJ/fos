'use strict';

// Lib
const moment = require(`moment-timezone`);
const phone = require(`phone`);
const bcrypt = require(`bcrypt`);

// Models
const User = require(`../models/User`);

// Services
const Utils = require(`../services/Utils`);

module.exports = {

  /**
   *
   * @api {get} /me My Profile
   * @apiName Myprofile
   * @apiGroup Me
   * @apiVersion  0.11.1
   * @apiPermission Customer, Apotek, Admin
   *
   * @apiSuccess (200) {Integer} id user
   * @apiSuccess (200) {String} email user
   * @apiSuccess (200) {String} fullname user
   * @apiSuccess (200) {String} handphone user
   *
   * @apiSuccessExample {type} Success-Response:
     {
      "meta": {
          "code": 200,
          "status": true,
          "messages": "Success"
      },
      "data": {
          "id": 3,
          "email": "customer@scode.com",
          "fullname": "Jaguar",
          "handphone": "0251212"
        }
    }
   *
   *
   */
  show: async(ctx, next) => {
    ctx.res.ok(ctx.req.user);
  },

  /**
   *
   * @api {put} /me Update my profile
   * @apiName Update Profile
   * @apiGroup Me
   * @apiVersion  0.11.1
   * @apiPermission Customer, Apotek, Admin
   *
   * @apiParam  {String} [email] Optional email of the user
   * @apiParam  {String} [fullname] Optional fullname of the user
   * @apiParam  {String} [username] Optional username of the user
   * @apiParam  {String} [password] Optional password of the user
   * @apiParam  {String} [handphone] Optional handphone of the user
   * @apiParam  {String} [address] Optional address of the user
   * @apiParam  {String} [gender] Optional gender of the user
   * @apiParam  {String} [date_of_birth] Optional date Of birth of the user
   *
   * @apiParamExample  {type} Request-Example:
   {
     "email":"test@gmail.com",
     "fullname":"test terbaru",
     "username":"test",
     "password":"12345678",
     "handphone":"+6289601848387",
     "address":"Jl. lebarku",
     "gender":"M",
     "date_of_birth":"1994-04-19",
     "image":"www.google.com"
    }
   *
   *
   * @apiSuccessExample {type} Success-Response:
     {
      "meta": {
          "code": 200,
          "status": true,
          "messages": "Ok"
      },
      "data": {
          "id": 3,
          "role_id": 3,
          "user_status_id": 1,
          "email": "customer@scode.com",
          "username": "test sajah",
          "fullname": "test test sajah",
          "handphone": "+6289601848387",
          "address": "Jl. Kiarasari Permai IV No. 24",
          "gender": "M",
          "date_of_birth": "1994-04-19T00:00:00.000Z",
          "confirm_code": null,
          "image": "www.google.com",
          "token_fcm": [
              {
                  "key": "android",
                  "value": "abc"
              }
          ],
          "created_at": 1497804941,
          "updated_at": 1500128623
      }
    }
   *
   *
   */
  update: async(ctx, next) => {
    const dataUser = await User.findOne({
      where: {
        id: ctx.req.user.id,
      },
    });

    if (Utils.checkNotNullValue(ctx.request.body.handphone)) {
      if (ctx.request.body.handphone.toLowerCase().replace(/ /g, ``).length < 9) {
        ctx.res.badRequest(`Panjang minimal handphone adalah 9 digit`);
      }

      if (ctx.request.body.handphone.indexOf(0) === 0 || ctx.request.body.handphone.indexOf(0) === `0`) {
        ctx.request.body.handphone = `${ctx.request.body.handphone.substr(0, 0)}+62${ctx.request.body.handphone.substr(1)}`;
      }

      if (phone(ctx.request.body.handphone, ``).length === 0) {
        ctx.res.badRequest(`Format handphone tidak valid`);
      }
    }

    if (Utils.checkNotNullValue(ctx.request.body.date_of_birth)) {
      if (!moment(ctx.request.body.date_of_birth, `YYYY-MM-DD`, true).isValid()) {
        ctx.res.badRequest(`Format tanggal lahir tidak valid`);
      }
    }

    // Check email validation
    if (Utils.checkNotNullValue(ctx.request.body.email)) {
      dataUser.email = ctx.request.body.email;
    }

    dataUser.fullname = ctx.request.body.fullname || dataUser.fullname;
    dataUser.username = ctx.request.body.username || dataUser.username;

    if (Utils.checkNotNullValue(ctx.request.body.password)) {
      dataUser.password = bcrypt.hashSync(ctx.request.body.password, 10);
    }

    dataUser.handphone = ctx.request.body.handphone || dataUser.handphone;
    dataUser.address = ctx.request.body.address || dataUser.address;
    dataUser.gender = ctx.request.body.gender || dataUser.gender;

    if (Utils.checkNotNullValue(ctx.request.body.image)) {
      dataUser.image = ctx.request.body.image;
    }

    if (Utils.checkNotNullValue(ctx.request.body.date_of_birth)) {
      dataUser.date_of_birth = `${ctx.request.body.date_of_birth} 00:00:00+00`;
    }

    dataUser.updated_at = Utils.getTimeEpoch();

    const saveDataUser = await dataUser.save();

    ctx.res.ok(saveDataUser);
  },

  /**
    *
    * @api {get} /me/order?access_token=<access_token>&offset=0&limit=10&sort=desc Get my order
    * @apiName Me Order
    * @apiGroup Me
    * @apiVersion  0.11.1
    * @apiPermission Customer
    *
    * @apiSuccess {Integer} id id of the  order.
    * @apiSuccess {String} user_id  name of the  .
    * @apiSuccess {Float} total  description of the  medicine.
    * @apiSuccess {Integer} apotek_id  type_id of the  medicine.
    * @apiSuccess {Integer} status  category_id of the  medicine.
    * @apiSuccess {String} resep  image of the  medicine.
    * @apiSuccess {String} payment_method  price of the  medicine.
    * @apiSuccess {String} address  apotek_id of the  medicine.
    * @apiSuccess {Float} lat  created_at of the  medicine.
    * @apiSuccess {Float} lng  updated_at of the  medicine.
    * @apiSuccess {Object} geom  created_by of the  medicine.
    * @apiSuccess {String} note  updated_by of the  medicine.
    * @apiSuccess {Integer} created_at  updated_by of the  medicine.
    * @apiSuccess {Integer} updated_at  updated_by of the  medicine.
    * @apiSuccess {Integer} created_by  updated_by of the  medicine.
    * @apiSuccess {Integer} updated_by  updated_by of the  medicine.
    *
    * @apiSuccessExample {type} Success-Response:
     {
    "meta": {
        "code": 200,
        "status": true,
        "messages": "Ok"
    },
    "data": [
        {
            "id": 2,
            "user_id": 6,
            "total": 2000,
            "apotek_id": null,
            "status": 0,
            "resep": null,
            "payment_method": "Cash",
            "address": "Jl. Kiaracondong No.99, Babakan Surabaya, Kiaracondong, Kota Bandung, Jawa Barat 40281, Indonesia",
            "lat": -6.91502298160217,
            "lng": null,
            "geom": {
                "type": "Point",
                "coordinates": [
                    0,
                    -6.915022981602174
                ]
            },
            "note": "",
            "created_at": 1499114578,
            "updated_at": 1499114578,
            "created_by": null,
            "updated_by": null
        }
      ]
    }
  *
  *
  */


  /**
   *
   * @api {get} /me/order/:id?access_token=<access_token> Get Detail order user
   * @apiName UserDetailOrder
   * @apiGroup Me
   * @apiVersion  0.13.0
   * @apiPermission Customer
   *
   * @apiSuccessExample {JSON} Success-Response:
   {
    "meta": {
        "code": 200,
        "status": true,
        "messages": "Ok"
    },
    "data": {
        "id": 13,
        "user_id": 3,
        "total": 2000,
        "apotek_id": null,
        "status": 0,
        "recipe": null,
        "payment_method": "",
        "address": "Jalan Kiaracondong, Babakan Surabaya, Bandung City, West Java, Indonesia",
        "lat": -6.9128645270155,
        "lng": 107.643653713167,
        "geom": {
            "type": "Point",
            "coordinates": [
                107.64365371316671,
                -6.912864527015499
            ]
        },
        "note": "RT 07/09",
        "created_at": 1499132417,
        "updated_at": 1499132417,
        "created_by": 3,
        "updated_by": 3,
        "user": {
            "id": 3,
            "role_id": 3,
            "user_status_id": 1,
            "email": "customer@scode.com",
            "username": null,
            "fullname": "Customer",
            "handphone": "00222111231",
            "address": null,
            "gender": null,
            "date_of_birth": null,
            "confirm_code": null,
            "token_fcm": [
                {
                    "key": "android",
                    "value": "abc"
                }
            ],
            "created_at": 1497804941,
            "updated_at": 1497804941
        },
        "menu_orders": [
            {
                "id": 7,
                "order_id": 13,
                "obat_id": 1,
                "qty": 4,
                "subtotal": 2000,
                "created_at": 1499132415,
                "updated_at": 1499132415,
                "created_by": null,
                "updated_by": null,
                "medicine": {
                    "id": 1,
                    "name": "Promag",
                    "description": "Obat untuk lambung",
                    "type_id": 1,
                    "category_id": 1,
                    "image": "https://sehat-q-development.s3-ap-southeast-1.amazonaws.com/images/category/1499113833_qLp3AnuUE9GZ7g1QUb4GvhN8lt4xcNKN8BLBhNwflKfrumvepbuI8V1M5Vr3ANBI03hPGOqKaGp1SMLcQD5BveryxaJigtNejszWJWhw2lItvU7hysKCPAYyBYMTPFAF.jpg",
                    "price": 500,
                    "apotek_id": 1,
                    "created_at": 1499111570,
                    "updated_at": 1499111570,
                    "created_by": 1,
                    "updated_by": 1
                }
            }
        ]
      }
    }
   *
   *
   */

  /**
   *
   * @api {get} /me/order/:id/rating?access_token=<access_token> Post rating order user
   * @apiName UserDetailOrder
   * @apiGroup Me
   * @apiVersion  0.13.0
   * @apiPermission Customer
   *
   * @apiSuccessExample {JSON} Success-Response:
   *
   *
   */
};
