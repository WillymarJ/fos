'use strict';

const Joi = require(`joi`);
const Promise = require(`bluebird`);

Joi.validateSync = Joi.validate;
Joi.validate = Promise.promisify(Joi.validate);

module.exports = Joi;
