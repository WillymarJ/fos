'use strict';

const passport = require(`koa-passport`);
const BearerStrategy = require(`passport-http-bearer`).Strategy;
const _ = require(`lodash`);

// Models
const OAuthAccessToken = require(`../../models/OAuthAccessToken`);
const User = require(`../../models/User`);
const Apotek = require(`../../models/Apotek`);

// Services
const Utils = require(`../../services/Utils`);

async function getAccessToken(accessToken) {
  return await OAuthAccessToken.findOne({ where: { access_token: accessToken } });
}

async function getDataUser(userId, clientId) {
  const dataUser = await User.findOne({ where: { id: userId } });
  const user = dataUser.toJSON();

  if (_.intersection([2], dataUser.role_id_all).length > 0 && clientId === 2) {
    user.apotek = await Apotek.findOne({ where: { user_id: userId } });
  }

  return user;
}

/**
 * This strategy is used to authenticate registered OAuth clients.
 * The authentication data must be delivered using the basic authentication scheme.
 */
passport.use(`accessToken`, new BearerStrategy((accessToken, done) => {
  getAccessToken(accessToken)
        .then((dataAccessToken) => {
          if (!dataAccessToken) return done(null, false);
          if (dataAccessToken.expired_in < Utils.getTimeEpoch()) return done(null, false);

          // Get data user
          getDataUser(dataAccessToken.user_id, dataAccessToken.client_id)
                .then((user) => {
                  user.client_id = dataAccessToken.client_id;
                  if (!user) return done(null, false);
                  const info = { scope: `*` };
                  delete user.password;
                  done(null, user, info);
                })
                .catch(err => done(err));
        })
        .catch(err => done(err));
}));
