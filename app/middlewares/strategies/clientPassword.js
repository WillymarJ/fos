'use strict';

const passport = require(`koa-passport`);
const ClientPasswordStrategy = require(`passport-oauth2-client-password`).Strategy;

// Models
const OAuthClient = require(`../../models/OAuthClient`);

async function getOauthClient(secretId, secret) {
  return await OAuthClient.findOne({ where: { secret_id: secretId, secret } });
}

/**
 * This strategy is used to authenticate registered OAuth clients.
 * The authentication data must be delivered using the strategy authentication scheme.
 */
passport.use(`clientPassword`, new ClientPasswordStrategy((clientId, clientSecret, done) => {
  getOauthClient(clientId, clientSecret)
        .then((user) => {
          if (clientId === user.get(`secret_id`) && clientSecret === user.get(`secret`)) {
            done(null, user.get());
          } else {
            done(null, false);
          }
        })
        .catch(err => done(err));
}));
