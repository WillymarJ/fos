'use strict';

const passport = require(`koa-passport`);
const BasicStrategy = require(`passport-http`).BasicStrategy;

// Models
const OAuthClient = require(`../../models/OAuthClient`);

async function getOauthClient(secretId, secret) {
  return await OAuthClient.findOne({ where: { secret_id: secretId, secret } });
}

/**
 * This strategy is used to authenticate registered OAuth clients.
 * The authentication data must be delivered using the basic authentication scheme.
 */
passport.use(`clientBasic`, new BasicStrategy((clientId, clientSecret, done) => {
  getOauthClient(clientId, clientSecret)
        .then((user) => {
          if (clientId === user.get(`secret_id`) && clientSecret === user.get(`secret`)) {
            done(null, user.get());
          } else {
            done(null, false);
          }
        })
        .catch(err => done(err));
}));
