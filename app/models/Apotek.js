'use strict';

const config = require(`config`);
const Sequelize = require(`sequelize`);
const User = require(`../models/User`);
const Database = require(`../helpers/database`);
const Utils = require(`../services/Utils`);

const Apotek = Database.define(`apotek`, {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  },
  user_id: {
    type: Sequelize.INTEGER,
  },
  password: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true,
      min: 6,
    },
  },
  name: {
    type: Sequelize.STRING,
  },
  address: {
    type: Sequelize.STRING,
  },
  image: {
    type: Sequelize.STRING,
  },
  no_telp: {
    type: Sequelize.STRING,
  },
  lat: {
    type: Sequelize.FLOAT,
  },
  lng: {
    type: Sequelize.FLOAT,
  },
  geom: {
    type: Sequelize.GEOMETRY(`POINT`, 4326),
  },
  license: {
    type: Sequelize.STRING,
  },
  wallet: {
    type: Sequelize.FLOAT,
    defaultValue: 0,
  },
  deposit: {
    type: Sequelize.FLOAT,
    defaultValue: 0,
  },
  in_order: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  is_online: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
  created_at: {
    type: Sequelize.INTEGER,
    defaultValue: Utils.getTimeEpoch(),
  },
  updated_at: {
    type: Sequelize.INTEGER,
    defaultValue: Utils.getTimeEpoch(),
  },
  created_by: {
    type: Sequelize.INTEGER,
  },
  updated_by: {
    type: Sequelize.INTEGER,
  },
}, {
  timestamps: false,
  freezeTableName: true,
  classMethods: {

    listApotekByDistance: async (data) => {
      const limit = data.limit || 10;
      const offset = data.offset || 0;
      const order = (data.order === `asc`) ? `ASC` : `DESC` || `DESC`;
      const radiusNotificationKm = data.radiusNotificationKm || config.radiusNotification.km;
      const radiusNotificationMeter = data.radiusNotificationMeter || config.radiusNotification.meter;

      return await Database.query(`SELECT "apotek"."id", "apotek"."deposit","apotek"."user_id",ST_Distance(geom, poi)/${radiusNotificationKm} AS distance_km
                                    FROM "apotek",
                                      (select ST_MakePoint(${data.lng},${data.lat})::geography as poi) as poi
                                    WHERE 
                                    ST_DWithin(geom, poi, ${radiusNotificationMeter}) 
                                    AND is_online = true
                                    AND in_order = false
                                    ORDER BY ST_Distance(geom, poi) ${order}
                                    OFFSET ${offset}
                                    LIMIT ${limit};`, {
                                      type: Database.QueryTypes.SELECT,
                                    });
    },
  },
});

Apotek.belongsTo(User, { foreignKey: `user_id` });
module.exports = Apotek;
