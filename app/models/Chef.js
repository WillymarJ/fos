'use strict';

const Sequelize = require(`sequelize`);
const User = require(`./User`);
const Database = require(`../helpers/database`);
const Utils = require(`../services/Utils`);

const Chef = Database.define(`chef`, {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  },
  user_id: {
    type: Sequelize.INTEGER,
  },
  password: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true,
      min: 6,
    },
  },
  name: {
    type: Sequelize.STRING,
  },
  address: {
    type: Sequelize.STRING,
  },
  image: {
    type: Sequelize.STRING,
  },
  in_order: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  is_online: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
  created_at: {
    type: Sequelize.INTEGER,
    defaultValue: Utils.getTimeEpoch(),
  },
  updated_at: {
    type: Sequelize.INTEGER,
    defaultValue: Utils.getTimeEpoch(),
  },
  created_by: {
    type: Sequelize.INTEGER,
  },
  updated_by: {
    type: Sequelize.INTEGER,
  },
}, {
  timestamps: false,
  freezeTableName: true,
});

Chef.belongsTo(User, { foreignKey: `user_id` });
module.exports = Chef;
