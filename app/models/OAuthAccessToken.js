'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../helpers/database`);
const Utils = require(`../services/Utils`);

const OAuthAccessToken = Database.define(`oauth_access_token`, {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  access_token: { type: Sequelize.STRING },
  client_id: { type: Sequelize.INTEGER },
  user_id: { type: Sequelize.INTEGER },
  expired_in: { type: Sequelize.INTEGER },
  created_at: { type: Sequelize.INTEGER, defaultValue: Utils.getTimeEpoch() },
  updated_at: { type: Sequelize.INTEGER, defaultValue: Utils.getTimeEpoch() },
}, {
  timestamps: false,
  freezeTableName: true,
});

module.exports = OAuthAccessToken;
