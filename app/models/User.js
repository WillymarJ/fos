'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../helpers/database`);
const Utils = require(`../services/Utils`);

const User = Database.define(`user`, {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  role_id: {
    type: Sequelize.INTEGER,
  },
  user_status_id: {
    type: Sequelize.INTEGER,
  },
  image: { type: Sequelize.STRING },
  email: {
    type: Sequelize.STRING,
    notEmpty: true,
    validate: {
      is: {
        args: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        msg: `Invalid email format`,
      },
      isUnique(value, next) {
        async function checkEmail(email) {
          return await User.find({
            where: { email },
            attributes: [`id`],
          });
        }
        checkEmail(value)
            .then((email) => {
              if (email) {
                const err = new Error(`Email address already in use!`);
                return next(err);
              }
              next();
            })
            .catch(err => next(err));
      },
    },
  },
  username: {
    type: Sequelize.STRING,
    notEmpty: true,
    validate: {
      is: {
        args: /([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,30}(?:[A-Za-z0-9_]))?)/g,
        msg: `Invalid username format`,
      },
      isUnique(value, next) {
        async function checkUsername(username) {
          return await User.find({
            where: { username },
            attributes: [`id`],
          });
        }
        checkUsername(value)
                .then((username) => {
                  if (username) {
                    const err = new Error(`Username address already in use!`);
                    return next(err);
                  }
                  next();
                })
                .catch(err => next(err));
      },
    },
  },
  password: {
    type: Sequelize.STRING,
    validate: {
      notEmpty: true,
      min: 6,
    },
  },
  fullname: {
    type: Sequelize.STRING,
  },
  handphone: {
    type: Sequelize.STRING,
    validate: {
      isNumeric: true,
    },
  },
  address: {
    type: Sequelize.TEXT,
  },
  gender: {
    type: Sequelize.ENUM(`M`, `F`),
    // validate: {
    //   isIn: {
    //     args: [[`M`, `F`]],
    //     msg: `M or F`,
    //   },
    // },
  },
  date_of_birth: {
    type: Sequelize.DATE,
  },
  confirm_code: {
    type: Sequelize.STRING,
  },
  role_id_all: {
    type: Sequelize.JSONB,
  },
  in_order: { type: Sequelize.BOOLEAN, defaultValue: false },
  created_at: { type: Sequelize.INTEGER, defaultValue: Utils.getTimeEpoch() },
  updated_at: { type: Sequelize.INTEGER, defaultValue: Utils.getTimeEpoch() },
}, {
  timestamps: false,
  freezeTableName: true,
  instanceMethods: {
    toJSON() {
      const values = Object.assign({}, this.get());

      delete values.password;
      return values;
    },
  },
});


module.exports = User;
