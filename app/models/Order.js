'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../helpers/database`);
const Utils = require(`../services/Utils`);


const Order = Database.define(`transactions`, {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  },
  notrx: {
    type: Sequelize.INTEGER,
  },
  userId: {
    type: Sequelize.INTEGER,
  },
  dishesId: {
    type: Sequelize.INTEGER,
  },
  portion: {
    type: Sequelize.INTEGER,
  },
  totalPrice: {
    type: Sequelize.INTEGER,
  },
  status: {
    type: Sequelize.STRING,
  },
  waitingTime: {
    type: Sequelize.INTEGER,
  },
  created_at: {
    type: Sequelize.INTEGER,
    defaultValue: Utils.getTimeEpoch(),
  },
  updated_at: {
    type: Sequelize.INTEGER,
    defaultValue: Utils.getTimeEpoch(),
  },
},
  {
    timestamps: false,
    freezeTableName: true,
  }
);

module.exports = Order;
