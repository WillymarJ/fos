'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../helpers/database`);
const Utils = require(`../services/Utils`);

const OAuthClient = Database.define(`oauth_client`, {
  id: { type: Sequelize.INTEGER, primaryKey: true },
  name: { type: Sequelize.STRING },
  website: { type: Sequelize.STRING },
  redirect_uri: { type: Sequelize.STRING },
  trusted: { type: Sequelize.BOOLEAN },
  description: { type: Sequelize.STRING },
  secret: { type: Sequelize.STRING },
  secret_id: { type: Sequelize.STRING },
  created_at: { type: Sequelize.INTEGER },
  updated_at: { type: Sequelize.INTEGER },
  created_by: { type: Sequelize.INTEGER },
  updated_by: { type: Sequelize.INTEGER },
}, {
  timestamps: false,
  freezeTableName: true,
});

module.exports = OAuthClient;
