'use strict';

const Sequelize = require(`sequelize`);
const Database = require(`../helpers/database`);
const Utils = require(`../services/Utils`);


const Menu = Database.define(`menu_dishes`, {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
  },
  description: {
    type: Sequelize.TEXT,
  },
  images: {
    type: Sequelize.STRING,
  },
  price: {
    type: Sequelize.STRING,
  },
  durationCook: {
    type: Sequelize.STRING,
  },
  isAvailable: {
    type: Sequelize.BOOLEAN,
  },
  created_at: {
    type: Sequelize.INTEGER,
    defaultValue: Utils.getTimeEpoch(),
  },
  updated_at: {
    type: Sequelize.INTEGER,
    defaultValue: Utils.getTimeEpoch(),
  },
},
  {
    timestamps: false,
    freezeTableName: true,
  }
);

module.exports = Menu;
