'use strict';

module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert(`role`, [
      {
        name: `Admin`,
        created_at: 1497804941,
        updated_at: 1497804941,
        // created_by: 1,
        // updated_by: 1,
      },
      {
        name: `Chef`,
        created_at: 1497804941,
        updated_at: 1497804941,
        // created_by: 1,
        // updated_by: 1,
      },
      {
        name: `Customer`,
        created_at: 1497804941,
        updated_at: 1497804941,
        // created_by: 1,
        // updated_by: 1,
      },

    ]);
  },

  down(queryInterface) {
    return queryInterface.bulkDelete(`role`, null, {});
  },
};
