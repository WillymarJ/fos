'use strict';

module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert(`user`, [
      {
        role_id: 1,
        user_status_id: 1,
        email: `admin@waldek.xyz`,
        fullname: `Admin`,
        password: `$2a$10$1IPvAhlqn6S3D.gnrCejz.HsYqVD332/JTn37Wc94MXfVyWRA/frG`, // 12345678
        handphone: `00222111231`,
        created_at: 1497804941,
        updated_at: 1497804941,
      },
      {
        role_id: 2,
        user_status_id: 1,
        email: `chef@waldek.xyz`,
        fullname: `Chef`,
        password: `$2a$10$1IPvAhlqn6S3D.gnrCejz.HsYqVD332/JTn37Wc94MXfVyWRA/frG`, // 12345678
        handphone: `00222111231`,
        created_at: 1497804941,
        updated_at: 1497804941,
      },
      {
        role_id: 3,
        user_status_id: 1,
        email: `customer@waldek.xyz`,
        fullname: `Customer`,
        password: `$2a$10$1IPvAhlqn6S3D.gnrCejz.HsYqVD332/JTn37Wc94MXfVyWRA/frG`, // 12345678
        handphone: `00222111231`,
        created_at: 1497804941,
        updated_at: 1497804941,
      },

    ]);
  },

  down(queryInterface) {
    return queryInterface.bulkDelete(`user`, null, {});
  },
};
