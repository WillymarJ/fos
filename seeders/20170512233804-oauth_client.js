'use strict';

module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert(`oauth_client`, [
      {
        secret_id: `QEHeENipdiAJ1Djnhv3ugU9SZ3wMGTB0LqFRP8a/pyc`,
        name: `android-user`,
        website: `http://waldek.xyz`,
        redirect_uri: `http://waldek.xyz`,
        trusted: `true`,
        description: `android-user`,
        secret: `KiOZ7bXzibFYfPlp0PXV90W2yhiSBVa7Ynhgbwmm8vU`,
        created_at: 1497804941,
        updated_at: 1497804941,
        created_by: 1,
        updated_by: 1,
      },
      {
        secret_id: `nhxrz+Lfr0v9Epuagfy4ygis7IMLXfk7VeBK0rrFg3VR`,
        name: `android-chef`,
        website: `http://waldek.xyz`,
        redirect_uri: `http://waldek.xyz`,
        trusted: `true`,
        description: `android-chef`,
        secret: `Jlk2ucBqnNHHR4blIZbzs3TFPCyXvQTNY2itNITNg/Zd`,
        created_at: 1497804941,
        updated_at: 1497804941,
        created_by: 1,
        updated_by: 1,
      },
      {
        secret_id: `UW9TA74rYYmADeG4EukuRAFZT0taM4a2usCwU/Yt/34=`,
        name: `cms`,
        website: `http://waldek.xyz`,
        redirect_uri: `http://waldek.xyz`,
        trusted: `true`,
        description: `cms`,
        secret: `XttdsLTzJpzMwu4kYAFY+I+NE5syeCdF8vwpgxl4v5A=`,
        created_at: 1497804941,
        updated_at: 1497804941,
        created_by: 1,
        updated_by: 1,
      },
    ]);
  },

  down(queryInterface) {
    return queryInterface.bulkDelete(`oauth_client`, null, {});
  },
};
