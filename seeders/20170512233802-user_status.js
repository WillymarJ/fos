'use strict';

module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert(`user_status`, [
      {
        name: `Active`,
        created_at: 1497804941,
        updated_at: 1497804941,
        // created_by: 1,
        // updated_by: 1,
      },
      {
        name: `Deactive`,
        created_at: 1497804941,
        updated_at: 1497804941,
        // created_by: 1,
        // updated_by: 1,
      },
      {
        name: `Blocked`,
        created_at: 1497804941,
        updated_at: 1497804941,
        // created_by: 1,
        // updated_by: 1,
      },

    ]);
  },

  down(queryInterface) {
    return queryInterface.bulkDelete(`user_status`, null, {});
  },
};
