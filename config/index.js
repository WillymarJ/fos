'use strict';

const _ = require(`lodash`);
const path = require(`path`);

module.exports = _.merge(require(`./`), {
  // dir
  appDir: path.join(__dirname, `..`),
  uploadDir: path.join(__dirname, `..`, `/assets/upload`),
  uploadDirImages: path.join(__dirname, `..`, `/assets/upload/images`),

});
