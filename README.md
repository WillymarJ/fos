# Food Order System
> A starter project with Node.js, Koa, Postgre

## Feature
Oauth2 grant password

## Docs
[https://documenter.getpostman.com/view/591935/RWgm3M49](https://documenter.getpostman.com/view/591935/RWgm3M49)

### Food Order System structure

```

├── README.md
├── app
│   ├── controllers
│   ├── helpers
│   ├── locales
│   ├── middlewares
│   ├─── strategies
│   ├── models
│   ├── routes
│   ├── services
│   ├── views
│   ├── worker
├── assets
│   ├── css
│   ├── img
│   ├── js
│   ├── upload
│   └── locales
├── bin
├── config
├── docs
└── test
│   ├── integration
│   ├── unit
│   ├── web
│   └── web demo
├── .jscsrc
├── app.js
├── bower.json
├── CHANGELOG.md
├── package.json
├── README.md
└── start
    

```

## Installation

__Install Node Modules__

`npm install`

then run:

```
CREATE DATABASE
edit config
Like 
./node_modules/.bin/sequelize db:migrate --env yourenvironment
./node_modules/.bin/sequelize db:seed:all --env yourenvironment
. start
```
