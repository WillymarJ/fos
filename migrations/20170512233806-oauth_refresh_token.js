'use strict';

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable(`oauth_refresh_token`, {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      refresh_token: Sequelize.STRING,
      client_id: {
        type: Sequelize.INTEGER,
        references: {
          model: `oauth_client`,
          key: `id`,
        },
        onUpdate: `cascade`,
        onDelete: `cascade`,
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: `user`,
          key: `id`,
        },
        onUpdate: `cascade`,
        onDelete: `cascade`,
      },
      expired_in: Sequelize.INTEGER,
      created_at: Sequelize.INTEGER,
      updated_at: Sequelize.INTEGER,
    });
  },

  down(queryInterface, Sequelize) {
    return queryInterface.dropTable(`oauth_refresh_token`);
  },
};
