'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(`transactions`, `waitingTime`, {
      type: Sequelize.INTEGER });
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn(`transactions`, `waitingTime`);
  },
};
