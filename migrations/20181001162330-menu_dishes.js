'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(`menu_dishes`, {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      description: {
        type: Sequelize.TEXT,
      },
      images: {
        type: Sequelize.STRING,
      },
      price: {
        type: Sequelize.STRING,
      },
      durationCook: {
        type: Sequelize.STRING,
      },
      isAvailable: {
        type: Sequelize.BOOLEAN,
      },
      created_at: {
        type: Sequelize.INTEGER },
      updated_at: {
        type: Sequelize.INTEGER },
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable(`menu_dishes`);
  },
};
