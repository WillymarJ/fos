'use strict';

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable(`user_status`, {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: Sequelize.STRING,
      created_at: Sequelize.INTEGER,
      updated_at: Sequelize.INTEGER,
    });
  },

  down(queryInterface, Sequelize) {
    return queryInterface.dropTable(`user_status`);
  },
};
