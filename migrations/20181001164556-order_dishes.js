'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(`transactions`, {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      notrx: { type: Sequelize.INTEGER },
      userId: { type: Sequelize.INTEGER },
      dishesId: { type: Sequelize.INTEGER },
      portion: { type: Sequelize.INTEGER },
      totalPrice: { type: Sequelize.INTEGER },
      status: { type: Sequelize.STRING },
      created_at: {
        type: Sequelize.INTEGER },
      updated_at: {
        type: Sequelize.INTEGER },
    });
  },

  down: (queryInterface) => {
    return queryInterface.dropTable(`transactions`);
  },
};
