'use strict';

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable(`user`, {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      role_id: {
        type: Sequelize.INTEGER,
        references: {
          model: `role`,
          key: `id`,
        },
        onUpdate: `cascade`,
        onDelete: `cascade`,
      },
      user_status_id: {
        type: Sequelize.INTEGER,
        references: {
          model: `user_status`,
          key: `id`,
        },
        onUpdate: `cascade`,
        onDelete: `cascade`,
      },
      email: Sequelize.STRING,
      image: Sequelize.STRING,
      username: Sequelize.STRING,
      in_order: Sequelize.BOOLEAN,
      fullname: Sequelize.STRING,
      password: Sequelize.STRING,
      handphone: Sequelize.STRING,
      address: Sequelize.TEXT,
      gender: Sequelize.ENUM(`M`, `F`),
      date_of_birth: Sequelize.DATE,
      confirm_code: Sequelize.STRING,
      role_id_all: Sequelize.JSONB,
      created_at: Sequelize.INTEGER,
      updated_at: Sequelize.INTEGER,
    });
  },

  down(queryInterface, Sequelize) {
    return queryInterface.dropTable(`user`);
  },
};
