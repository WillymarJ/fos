'use strict';

module.exports = {
  up(queryInterface, Sequelize) {
    return queryInterface.createTable(`oauth_client`, {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      secret_id: {
        type: Sequelize.STRING,
      },
      name: {
        type: Sequelize.STRING,
      },
      website: {
        type: Sequelize.STRING,
      },
      redirect_uri: {
        type: Sequelize.STRING,
      },
      trusted: {
        type: Sequelize.STRING,
      },
      description: {
        type: Sequelize.STRING,
      },
      secret: {
        type: Sequelize.STRING,
      },
      created_at: Sequelize.INTEGER,
      updated_at: Sequelize.INTEGER,
      created_by: {
        type: Sequelize.INTEGER,
        references: {
          model: `user`,
          key: `id`,
        },
        onUpdate: `cascade`,
        onDelete: `cascade`,
      },
      updated_by: {
        type: Sequelize.INTEGER,
        references: {
          model: `user`,
          key: `id`,
        },
        onUpdate: `cascade`,
        onDelete: `cascade`,
      },
    });
  },

  down(queryInterface, Sequelize) {
    return queryInterface.dropTable(`oauth_client`);
  },
};
